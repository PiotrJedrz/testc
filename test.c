#include <stdio.h>
#include <stdbool.h>
#include <math.h>

//Zad.1
bool isAnagram(char first[], char second[], int lengthFirst, int lengthSecond)
{
    if(lengthFirst != lengthSecond)
        return false;

    bool flag = false;
    int i,j;

    int repeatTest[lengthFirst];

    for(i = 0; i < lengthFirst; i++)
    {
        flag = false;

        for(j = 0; j < lengthSecond; j++)
        {

            if(first[i] == second[j] && repeatTest[j] != 1)
            {
                flag = true;
                repeatTest[j] = 1;
                break;
            }
        }

        if(!flag)
            return false;
    }

    return true;
}

//Zad.2
//Strukture p wykorzystuje w zad.6, deklaruje sobie teraz dla wygody
struct point{
    int x;
    int y;
    int z;
};
struct list{
    int id;
    struct point p;
    struct list* next;
};

struct list* AddToList(struct list* head, int newID, struct point newP)
{
    struct list *new = (struct list*) malloc(sizeof(struct list));
    new->id = newID;
    new->next = NULL;
    new->p = newP;

    if(head == NULL)
        head = new;
    else
    {
        struct list *temp = head;

        while(temp->next != NULL)
        {
            temp = temp->next;
        }

        temp->next = new;
        
    }

    return head;
}

struct list* DeleteFromList(struct list* head, int nToDelete)
{
    int n = 1;
    struct list* temp = head;
    struct list* prev = NULL;

    if(nToDelete == 1)
    {
        if(head->next != NULL)
        {
            temp = head->next;
            free(head);
            return temp;
        }
        else
        {
            free(head);
            return NULL;
        }
    }

    while(temp != NULL)
    {
        if(n != nToDelete)
        {
            prev = temp;
            temp = temp->next;
            n++;
        }
        else
        {
            prev->next = temp->next;
            free(temp);
            break;
        }
        
    }

    return head;
}

struct list* FindInLIst(struct list* head, int nToFind)
{
    int n = 1;
    
    while(head != NULL)
    {
        if(n == nToFind)
        {
            printf("Element %d found with value %d\n", nToFind, head->id);
            return head;
        }
        else
        {
            head = head->next;
            n++;
        }
    }

    printf("List too short\n");
    return NULL;

}

void ShowList(struct list* head)
{
    if(head == NULL)
    {
        printf("List empty");
    }
    else
    {
        while(head != NULL)
        {
            printf("%d ", head->id);
            head = head->next;
        }
    }

    printf("\n");
}

//Zad.3
int SumOfElements(int* array, int sizeOfArray)
{
    int max = -INFINITY;
    int min = INFINITY;

    int i, sum = 0;
    for(i = 0; i < sizeOfArray; i++)
    {
        sum += array[i];

        if(array[i] > max)
            max = array[i];

        if(array[i] < min)
            min = array[i];
    }

    return sum - (max + min);
}

//Zad.4

int GetStretchedSize(int n)
{
    int i, k = 1;

    for(i = 2; i < n+1; i++)
    {
        k += 1 + i;
    }

    return k;
}

void Stretch(char *array, char *stretchedArray, int sizeOfArray)
{
    int i,j,n = 0;

    for(i = 0; i < sizeOfArray; i++)
    {
        for(j = 0; j < i + 1; j++)
        {
            if(j == 0)
            {
                stretchedArray[n] = array[i] - 32;
                n++;
            }
            else
            {
                stretchedArray[n] = array[i];
                n++;
            }
            
            
        }
            stretchedArray[n] = '-';
            n++;
    }

}

//Zad.5

int IsSorted(int array[], int arraySize)
{
    int i;
    bool rising = true, falling = true;

    for(i = 1; i < arraySize; i++)
    {
        if(array[i] >= array[i-1])
        {
            falling = false;
        }
        else if(array[i] <= array[i-1])
        {
            rising = false;
        }

        if(!rising && !falling)
            return 0;
    }

    if(rising && !falling)
        return 1;
    else if(!rising && falling)
        return -1;
    else
        return 0;
        
}

//Zad.6

void CountPoints(struct list* pointHead)
{
    int i = 0 ;
    while(pointHead != NULL)
    {
        i++;
        pointHead = pointHead->next;
    }

    printf("Point count: %d\n\n", i);
}

struct list* PropagateGas(struct list* point, bool*** cube, int n)
{
    int i;
    int px = point->p.x;
    int py = point->p.y;
    int pz = point->p.z;

    for(i = -1; i < 2; i+= 2)
    {  
        if(px + i > -1 && px + i < n && cube[px + i][py][pz] == false)
        {
            cube[px + i][py][pz] = true;

            struct point p;
            p.x = px + i; p.y = py, p.z = pz;
            point = AddToList(point, point->id + 1, p);
        }
        if(py + i > -1 && py + i < n && cube[px][py + i][pz] == false)
        {
            cube[px][py + i][pz] = true;

            struct point p;
            p.x = px; p.y = py + i, p.z = pz;
            point = AddToList(point, point->id + 1, p);
        }
        if(pz + i > -1 && pz + i < n && cube[px][py][pz + i] == false)
        {
            cube[px][py][pz + i] = true;

            struct point p;
            p.x = px; p.y = py, p.z = pz + i;
            point = AddToList(point, point->id + 1, p);
        }
    }

    point = DeleteFromList(point, 1);
    return point;
}

void SimulateCube(int n, struct point startP, struct point endP)
{
    int i,j,k;
    bool*** cube = malloc(sizeof(bool*) * n);
    bool found = false;
    
    struct list* pointsHead = NULL;

    for(i = 0; i < n; i++)
    {
        cube[i] = malloc(sizeof(bool*) * n);
        for( j = 0; j < n; j++)
        {
            cube[i][j] = malloc(sizeof(bool*) * n);
            for(k = 0; k < n; k++)
            {
                cube[i][j][k] = malloc(sizeof(bool) * n);
                cube[i][j][k] = false;
            }
        }
    }

    i = 0;
    pointsHead = AddToList(pointsHead, i, startP);
    int px = pointsHead->p.x;
    int py = pointsHead->p.y;
    int pz = pointsHead->p.z;
    cube[px][py][pz] = true;

    ShowCube(cube, n);

    while(pointsHead != NULL || !found)
    {
        //struct list* tempHead = pointsHead;
        pointsHead = PropagateGas(pointsHead, cube, n);

        if(i != pointsHead->id)
        {
            ShowCube(cube, n);
            i++;
        }

        if(pointsHead->p.x == endP.x && pointsHead->p.y == endP.y && pointsHead->p.z == endP.z)
            found = true;

        if(pointsHead->next == NULL || found)
            break;

    }
    

    printf("It took %d seconds", i);
}

void ShowCube(bool*** cube, int n)
{
    int i, j, k;

    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            for(k = 0; k < n; k++)
            {
                printf("%d", cube[i][j][k]);
            }
            printf("\n");
        }
        printf("\n");
    }

    printf("----------\n");
}


int main()
{
    
    int i;
    //Zad.1
    printf("Zad.1\n");
    char first[] = { 'h', 'a', 'm', 'i', 'l', 'l', '\0'};
    char second[] = { 'l', 'i', 'm', 'a','h', 'l', '\0'};
    char third[] = { 'l', 'i', 'm', 'a','l', 'l', '\0'};

    printf("Is %s an anagram of %s? Result %d\n\n", first, second, isAnagram(first, second, 6, 6));
    printf("Is %s an anagram of %s? Result %d\n\n", first, third, isAnagram(first, second, 6, 6));

    //Zad.2
    printf("Zad.2\n");
    struct list* head = NULL;
    struct point temp;
    ShowList(head);
    head = AddToList(head, 2, temp);
    head = AddToList(head, 5, temp);
    head = AddToList(head, 4, temp);
    head = AddToList(head, 1, temp);
    head = AddToList(head, 8, temp);
    ShowList(head);
    head = DeleteFromList(head, 2);
    ShowList(head);
    head = DeleteFromList(head, 1);
    ShowList(head);
    FindInLIst(head, 3);
    FindInLIst(head, 5);
    printf("\n");

    //Zad.3
    printf("Zad.3\n");
    int array3[] = {1, 2, 3, 4, 6};
    printf("Sum of elements: %d\n", SumOfElements(&array3, 5));

    printf("\n");

    //Zad.4
    printf("Zad.4\n");
    char *word = (char*) malloc(sizeof(char) * 4);
    char *stretchedWord = (char*) malloc(sizeof(char) * GetStretchedSize(4));
    word[0] = 'p';
    word[1] = 'i';
    word[2] = 'e';
    word[3] = 's';
    
    Stretch(word, stretchedWord, 4);

    for(i = 0; i < GetStretchedSize(4); i++)
    {
        printf("%c", stretchedWord[i]);
    }
    
    printf("\n");
    printf("\n");

    //Zad.5
    printf("Zad.5\n");
    int array5[] = {5, 4, 3, 2, 1};

    printf("Funkcja zwraca %d dla array3\n", IsSorted(array3, 5));
    printf("Funkcja zwraca %d dla array5\n", IsSorted(array5, 5));

    printf("\n");
    
    //Zad.6
    printf("Zad.6\n");
    int n = 3;
    struct point startP;
    struct point endP;
    startP.x = 0; startP.y = 0; startP.z = 0;
    endP.x = n - 1; endP.y = 0; endP.z = n - 1;
    SimulateCube(n, startP, endP);
    

    return 0;
}